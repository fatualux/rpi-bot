#!/bin/bash

CONFIG_FILE="config.py"
echo -n "Enter your Telegram bot token: "
read -r token
echo -n "Enter your Telegram account ID: "
read -r admin
echo -n "Enter the path to your home directory: "
read -r home_dir
echo "bot_token = '$bot_token'" >> $CONFIG_FILE
echo "admin = '$admin'" >> $CONFIG_FILE
echo "home_dir = '$home_dir/Media/'" >> $CONFIG_FILE
echo "Config file created."
echo "Configuration written to $CONFIG_FILE"
