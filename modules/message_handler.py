import telepot
import sys
from modules.custom_kbd import CustomKeyboard
from modules.start_command import StartCommand
from modules.motion_on_command import MotionOnCommand
from modules.motion_off_command import MotionOffCommand
from modules.snapshot_command import SnapshotCommand
from modules.video_command import VideoCommand
from modules.audio_command import AudioCommand
from modules.del_command import DelCommand
from modules.poweroff_command import PowerOffCommand
from modules.reboot_command import RebootCommand
import config as cfg

sys.path.append('modules')
sys.path.append('..')


class MessageHandler:
    def __init__(self, bot):
        self.bot = bot

    def handle_message(self, msg):
        usr_name = msg['from']['first_name']
        usr_id = msg["from"]["id"]
        content_type, _, chat_id = telepot.glance(msg)
        if content_type == 'text':
            command = msg['text'].lower()
            if usr_id != int(cfg.admin):
                message = 'Sorry ' + usr_name + ', unauthorized access.'
                self.bot.sendMessage(chat_id, message)
            else:
                command_map = {
                    "/start": StartCommand,
                    "on": StartCommand,
                    "/motionon": MotionOnCommand,
                    "motion on": MotionOnCommand,
                    "/motionoff": MotionOffCommand,
                    "motion off": MotionOffCommand,
                    "/snapshot": SnapshotCommand,
                    "snapshot": SnapshotCommand,
                    "/videorec": VideoCommand,
                    "video": VideoCommand,
                    "/audiorec": AudioCommand,
                    "audio": AudioCommand,
                    "/delete": DelCommand,
                    "delete all": DelCommand,
                    "/reboot": RebootCommand,
                    "reboot": RebootCommand,
                    "/poweroff": PowerOffCommand,
                    "poweroff": PowerOffCommand,
                }
                if command in command_map:
                    command_obj = command_map[command](self.bot, chat_id)
                    command_obj.execute()
                else:
                    message = 'Sorry, I do not understand. Write me some text.'
                    self.bot.sendMessage(
                        chat_id, message, reply_markup=CustomKeyboard.show_keyboard()
                    )
