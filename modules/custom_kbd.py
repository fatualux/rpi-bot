class CustomKeyboard:
    @staticmethod
    def hide_keyboard():
        return {'hide_keyboard': True}

    @staticmethod
    def show_keyboard():
        return {'keyboard': [['Motion ON', 'Motion OFF'],
                             ['Audio', 'Snapshot', 'Video'],
                             ['Delete All', 'REBOOT'],
                             ['POWEROFF']]}
