import os
from modules.custom_kbd import CustomKeyboard
from modules.command import Command

class RebootCommand(Command):
    def execute(self):
        os.system('sudo reboot')
        message = 'Riavvio sistema.'
        self.bot.sendMessage(self.chat_id, message, reply_markup=CustomKeyboard.hide_keyboard())
