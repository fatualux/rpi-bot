from modules.command import Command
from modules.custom_kbd import CustomKeyboard


class StartCommand(Command):
    def execute(self):
        msg = 'Hi! What can I do for you?'
        self.bot.sendMessage(
            self.chat_id, msg, reply_markup=CustomKeyboard.show_keyboard()
        )
