import os
from modules.command import Command
from modules.custom_kbd import CustomKeyboard

class MotionOffCommand(Command):
    def execute(self):
        os.system('sudo pkill motion')
        message = 'Ho disattivato il Guardian Mode.'
        self.bot.sendMessage(self.chat_id, message, reply_markup=CustomKeyboard.show_keyboard())
        event = "Motion detection deactivated."
        print(event)
        log_write(0, event)
