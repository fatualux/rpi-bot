import os
import sys
import sounddevice as sd
import soundfile as sf
import threading
from modules.custom_kbd import CustomKeyboard
import config as cfg
import time

sys.path.append('modules')
sys.path.append('..')

audio_path = f'{cfg.home_dir}/Audio/audio.wav'

class AudioCommand:
    def __init__(self, bot, chat_id):
        self.bot = bot
        self.chat_id = chat_id
        self.stop_recording = threading.Event()

    def execute(self):
        # create audio path if it doesn't exist
        if not os.path.exists(os.path.dirname(audio_path)):
            os.makedirs(os.path.dirname(audio_path))

        duration = 10  # Duration of the audio recording in seconds
        sample_rate = 44100  # Sample rate for audio recording

        print("Recording audio...")
        # Start recording in a separate thread
        recording_thread = threading.Thread(target=self.record_audio, args=(duration, sample_rate))
        recording_thread.start()

        # Wait for recording to finish or timeout
        recording_thread.join(duration)

        # Signal the recording thread to stop
        self.stop_recording.set()

        # Send message about the recording status
        if os.path.exists(audio_path):
            self.send_audio(audio_path)
        else:
            msg = "Failed to record audio."
            self.bot.sendMessage(
                self.chat_id, msg, reply_markup=CustomKeyboard.show_keyboard()
            )

    def record_audio(self, duration, sample_rate):
        try:
            start_time = time.time()
            recording = sd.rec(int(duration * sample_rate), samplerate=sample_rate, channels=2)
            while time.time() - start_time < duration and not self.stop_recording.is_set():
                time.sleep(0.1)  # Check stop condition every 0.1 seconds
            sd.stop()
            sd.wait()
            sf.write(audio_path, recording, sample_rate)
            print("Recording completed.")
        except Exception as e:
            print("Error recording audio:", e)

    def send_audio(self, audio_path):
        # Send the recorded audio file to the user
        try:
            with open(audio_path, 'rb') as audio_file:
                self.bot.sendAudio(self.chat_id, audio_file)
                msg = "Audio sent successfully."
                self.bot.sendMessage(
                    self.chat_id, msg, reply_markup=CustomKeyboard.show_keyboard()
                )
        except Exception as e:
            print("Error sending audio:", e)
            self.bot.sendMessage(self.chat_id, "Failed to send audio.")
