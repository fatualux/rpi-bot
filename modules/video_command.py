import cv2
import time
import os
from modules.custom_kbd import CustomKeyboard
import sys
import config as cfg

video_path = f'{cfg.home_dir}/Video/video.mp4'

sys.path.append('modules')
sys.path.append('..')


class VideoCommand:
    def __init__(self, bot, chat_id):
        self.bot = bot
        self.chat_id = chat_id

    def execute(self):
        # create video path if it doesn't exist
        if not os.path.exists(os.path.dirname(video_path)):
            os.makedirs(os.path.dirname(video_path))
        duration = 10
        interval = 0.2

        # OpenCV VideoWriter settings
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        out = cv2.VideoWriter(video_path, fourcc, 5.0, (640, 480))

        # Capture video for the specified duration
        start_time = time.time()
        while time.time() - start_time < duration:
            # Capture frame from the camera
            ret, frame = self.capture_frame()
            if not ret:
                msg = "Failed to capture video."
                self.bot.sendMessage(self.chat_id, msg)
                return

            # Write the frame to the output video
            out.write(frame)

            # Pause for the specified interval
            time.sleep(interval)

        # Release the video writer
        out.release()

        # Send the recorded video file
        self.send_video(video_path)

    def capture_frame(self):
        # Attempt to capture frame from the camera
        camera = cv2.VideoCapture(0)  # Use device index 0 for the first camera
        ret, frame = camera.read()
        camera.release()
        return ret, frame

    def send_video(self, video_path):
        # Send the recorded video file to the user
        try:
            with open(video_path, 'rb') as video_file:
                self.bot.sendVideo(self.chat_id, video_file)
        except Exception as e:
            print("Error sending video:", e)
            self.bot.sendMessage(self.chat_id, "Failed to send video.")
        finally:
            # Remove the video file after sending
            msg = "Video sent successfully."
            self.bot.sendMessage(
                self.chat_id, msg, reply_markup=CustomKeyboard.show_keyboard()
            )
