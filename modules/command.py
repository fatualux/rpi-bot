# command.py
class Command:
    def __init__(self, bot, chat_id):
        self.bot = bot
        self.chat_id = chat_id

    def execute(self):
        raise NotImplementedError("Subclasses must implement execute method")
