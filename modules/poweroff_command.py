import os
from modules.command import Command
from modules.custom_kbd import CustomKeyboard  # Importing the CustomKeyboard class

class PowerOffCommand(Command):
    def execute(self):
        os.system('sudo poweroff')
        message = 'Spegnimento sistema.'
        self.bot.sendMessage(self.chat_id, message, reply_markup=CustomKeyboard.hide_keyboard())  # Using hide_keyboard from CustomKeyboard
