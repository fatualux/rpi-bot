import cv2
import os
import sys
from modules.custom_kbd import CustomKeyboard
import config as cfg

sys.path.append('modules')
sys.path.append('..')

snap_path = f'{cfg.home_dir}/Pictures/snapshot.jpg'


class SnapshotCommand:
    def __init__(self, bot, chat_id):
        self.bot = bot
        self.chat_id = chat_id

    def execute(self):
        # create snapshot path if it doesn't exist
        if not os.path.exists(os.path.dirname(snap_path)):
            os.makedirs(os.path.dirname(snap_path))
        # Try capturing from the RPi camera
        # Use device index 0 for the first camera
        rpi_camera = cv2.VideoCapture(0)
        rpi_camera.set(cv2.CAP_PROP_FRAME_WIDTH, 800)
        rpi_camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 600)
        # Check if the RPi camera is available
        if rpi_camera.isOpened():
            ret, frame = rpi_camera.read()
            if ret:
                cv2.imwrite(snap_path, frame)
                msg = "Snapshot taken successfully with RPi camera."
            else:
                msg = "Failed to capture snapshot with RPi camera."
            rpi_camera.release()
        else:
            # If RPi camera is not available,
            # try capturing from internal or USB camera
            # Use device index 1 for the internal camera
            internal_camera = cv2.VideoCapture(1)
            internal_camera.set(cv2.CAP_PROP_FRAME_WIDTH, 800)
            internal_camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 600)
            # Check if the internal camera is available
            if internal_camera.isOpened():
                ret, frame = internal_camera.read()
                if ret:
                    cv2.imwrite(snap_path, frame)
                    msg = "Snapshot taken successfully with internal camera."
                else:
                    msg = "Failed to capture snapshot with internal camera."
                internal_camera.release()
            else:
                msg = "No camera available to capture snapshot."
        # Send message about the snapshot status
        self.bot.sendMessage(self.chat_id, msg)

        # Send the captured snapshot image
        try:
            with open(snap_path, 'rb') as snap_file:
                self.bot.sendPhoto(self.chat_id, snap_file)
        except Exception as e:
            msg = "Failed to send snapshot."
            print(msg, e)
            self.bot.sendMessage(self.chat_id, msg)
        finally:
            msg = "Snapshot sent successfully."
            self.bot.sendMessage(
                self.chat_id, msg, reply_markup=CustomKeyboard.show_keyboard()
            )
