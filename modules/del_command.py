import os
from modules.command import Command
from modules.custom_kbd import CustomKeyboard
import sys
import config as cfg

sys.path.append('modules')
sys.path.append('..')


class DelCommand(Command):
    def execute(self):
        path = cfg.home_dir
        os.system('rm ' + path + 'Audio/*')
        os.system('rm ' + path + 'Video/*')
        os.system('rm ' + path + 'Pictures/*')
        msg = "All media files have been deleted."
        self.bot.sendMessage(
            self.chat_id, msg, reply_markup=CustomKeyboard.show_keyboard()
        )
