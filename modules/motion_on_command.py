import os
from modules.command import Command
from modules.custom_kbd import CustomKeyboard

class MotionOnCommand(Command):
    def execute(self):
        message = 'Ho attivato il Guardian Mode.'
        self.bot.sendMessage(self.chat_id, message, reply_markup=CustomKeyboard.hide_keyboard())
        event = "Motion detection activated."
        print(event)
        os.system('motion -b')
