import os
from modules.message_handler import MessageHandler
import telepot
import config as cfg

# Initialize the Telegram bot
bot = telepot.Bot(cfg.token)

# Initialize the message handler
message_handler = MessageHandler(bot)

# Function to handle incoming messages
def handle(msg):
    message_handler.handle_message(msg)

# Start listening for messages
bot.message_loop(handle)

# Keep the script running
while True:
    pass
