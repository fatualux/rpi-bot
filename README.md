![PIC](./demo/demo.png)

### This is a simple python Telegram bot initially designed to run on a Raspberry Pi.

## REQUIREMENTS

[![Python](https://img.shields.io/badge/Python-3.7-blue.svg)](https://www.python.org/downloads/) or higher

## INSTALLATION

### Running the app within a Python virtual environment (recommended)

Note: for this procedure to work, you need to have ***virtualenv*** installed.

```
git clone https://www.gitlab.com:fatualux/rpi-bot && cd rpi-bot
sh venv.sh
```

### Running the app outside a virtual environment

```
git clone https://www.gitlab.com:fatualux/rpi-bot && cd rpi-bot
```

You can install all the needed dependencies with the following command:

```
python -m pip install -r requirements.txt
```

Create a config file:

```
sh gen-config.sh
```

## USAGE

```
python rpi-bot.py
```

Done!

## LICENSE

[![License](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

This project is licensed under the GPLv3 license.
See LICENSE file for more details.
